ARG FROM_IMAGE=maven:3.5.3-jdk-8

FROM ${FROM_IMAGE}

ARG DEBIAN_DOCKER_VERSION=stretch
ARG DOCKER_VERSION=5:19.03.0~3-0~debian-${DEBIAN_DOCKER_VERSION}
ARG DOCKER_ARCH=amd64

ENV CACERTS ${JAVA_HOME}/jre/lib/security/cacerts

RUN curl -fsSL https://download.docker.com/linux/debian/gpg  | apt-key add - 

# The Debian release can be obtained with lsb_release -cs
# You can substitute $DEBIAN_DOCKER_VERSION by $(lsb_release -cs)
# but Docker is not always available for latest Debians
RUN apt-get update && \
	apt autoremove && \
    apt-get upgrade -y && \
    apt-get install -y apt-transport-https ca-certificates \
		            curl gnupg2 software-properties-common && \
    add-apt-repository \
     "deb [arch=${DOCKER_ARCH}] https://download.docker.com/linux/debian \
     ${DEBIAN_DOCKER_VERSION} stable" && \
    apt-get update && echo "List of available Docker versions ( we are using ${DOCKER_VERSION} )" && apt-cache madison docker-ce-cli && \
    apt-get install -y docker-ce-cli=${DOCKER_VERSION} && \
    apt clean -y && rm -rf /var/lib/apt/lists/ 

RUN   wget -O /tmp/cern-root-ca-2.crt "https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt" && \
      wget -O /tmp/cern-ca.crt        "https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority.crt" && \
      wget -O /tmp/cern-ca-1.crt      "https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority(1).crt" && \
      wget -O /tmp/cern-grid-ca.crt      "https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_ROOT_CA_2 -file "/tmp/cern-root-ca-2.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_CA -file "/tmp/cern-ca.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_CA_1 -file "/tmp/cern-ca-1.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_GRID_CA -file "/tmp/cern-grid-ca.crt" && \
      mkdir -p ~/.ssh && \
      /bin/bash -c 'echo -e "Host gitlab.cern.ch\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config' && \
      rm /tmp/*.crt

RUN mkdir -p /opt
ADD src/main/java/SSLPoke.java /opt
RUN cd /opt && javac SSLPoke.java && java SSLPoke repository.cern.ch 443


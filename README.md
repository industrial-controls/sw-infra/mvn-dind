# Maven Docker in Docker

An official Maven Debian + Docker + CERN CA certificates image to perform Docker-in-Docker builds on CERN Gitlab.

The image build is provided :

* As a *baseline image*, with default build arguments hard-coded in the provided Dockerfile. Consult the Dockerfile to know the current configuration.
* As a *Maven-enabled image* with support for gitflow conventions, branching, parameters and releases (thanks to the maven-dockerfile-plugin).

*Important Note :* The Gitlab CI only performs a baseline build in the master branch. The baseline is used to rebuild all other images so pick a sensible default.

# How to update the baseline

Simply update the Dockerfile build arguments' default values, and commit to the ```master``` branch. NOTE : The baseline is used to rebuild all other images so pick a sensible default.

# How to provide alternate images

Using gitflow, you can create a new *release* branch, it will automatically be picked up and deployed as a new docker image.

E.g., to create a new release branch for Maven 3.5.3, Docker 19.3.0 and Jdk11, simply execute : 
```
git flow release start 3.5.3-19.3.0-jdk11 develop
```

To name your release branch, please follow the convention 
```<maven-version>-<docker-client-version>-<jdk-version>```


For instance :

* 3.5.3-19.0.3-jdk8
* 3.6.0-19.0.3.2-jdk11
